export interface RegisterCardResponse {
    cardId: number;
    isFailure: boolean;
    problemDescription: string;
}