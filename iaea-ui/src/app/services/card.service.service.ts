import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { RegisterCardRequest } from '../models/register-card-request';
import { RegisterCardResponse } from '../models/register-card-response';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  private serverUrl = 'https://localhost:5001/api/card';

  constructor(private httpClient: HttpClient) { }

  public registerCard(visitorName: string): Observable<RegisterCardResponse> {
    const requestData: RegisterCardRequest = {
      visitorName
    } as RegisterCardRequest;
    return this.httpClient.post<RegisterCardResponse>(this.serverUrl, requestData);
  }
}
