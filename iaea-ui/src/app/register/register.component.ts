import { Component, OnInit } from '@angular/core';
import { CardService } from '../services/card.service.service';

@Component({
  selector: 'iaea-ui-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public visitorName: string;

  constructor(private cardService: CardService) { }

  ngOnInit() {
  }

  public onRegisterCard(): void {
    this.cardService.registerCard(this.visitorName).subscribe(response => {
        if (response.isFailure) {
          alert(response.problemDescription);
        } else {
          alert(`Cand added with Id ${response.cardId}`);
        }
      });
    this.visitorName = '';
  }
}
