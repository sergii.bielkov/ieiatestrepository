﻿namespace IAEA.Api.Models
{
    public class RegisterCardResponse
    {
        public int CardId { get; set; }

        public bool IsFailure { get; set; }

        public string ProblemDescription { get; set; }
    }
}