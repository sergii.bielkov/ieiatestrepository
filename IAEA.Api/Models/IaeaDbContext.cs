﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace IAEA.Api.Models
{
    public class IaeaDbContext : DbContext
    {
        public IaeaDbContext(DbContextOptions<IaeaDbContext> options)
         : base(options)
        {
        }

        public DbSet<Visitor> Visitors { get; set; }
        public DbSet<Card> Cards { get; set; }

        /// <summary>
        /// Seeding data.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Card>()
                .HasOne(card => card.Visitor)
                .WithMany(visitor => visitor.Cards)
                .HasForeignKey(card => card.VisitorId);

            // Seed data.
            modelBuilder.Entity<Visitor>().HasData(new Visitor
            {
                Id = 1,
                Name = "Superman"
            });
        }
    }
}
