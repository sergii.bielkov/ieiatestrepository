﻿using System.ComponentModel.DataAnnotations.Schema;

namespace IAEA.Api.Models
{
    public class Card
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public bool IsActive { get; set; }

        public bool IsCheckedIn { get; set; }

        public int VisitorId { get; set; }
        public Visitor Visitor { get; set; }
    }
}
