﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace IAEA.Api.Models
{
    public class Visitor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Card> Cards { get; set; }
    }
}
