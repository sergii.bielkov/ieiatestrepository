﻿using System.Threading.Tasks;
using IAEA.Api.Models;

namespace IAEA.Api.Services
{
    public interface ITrackingService
    {
        Task<Card> RegisterCardAsync(string visitorName);
    }
}
