﻿using System;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using IAEA.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace IAEA.Api.Services
{
    public class TrackingService : ITrackingService
    {
        private readonly IaeaDbContext _dbContext;
        private readonly ILogger<TrackingService> _logger;

        public TrackingService(IaeaDbContext dbContext, ILogger<TrackingService> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
            _dbContext.Database.EnsureCreated();
        }

        public Task<Card> RegisterCardAsync(string visitorName)
        {
            _logger.LogInformation($"Call RegisterCardAsync for visitor {visitorName}");
            if (string.IsNullOrEmpty(visitorName))
            {
                var message = "Visitor name can not be empty.";
                _logger.LogError(message);
                throw new ArgumentException(message, nameof(visitorName));
            }

            var visitor = _dbContext.Visitors.FirstOrDefault(visitorItem => visitorItem.Name == visitorName);
            if (visitor == null)
            {
                var message = "Visitor does not exist. Please specify valid name.";
                _logger.LogError(message);
                throw new ArgumentException(message);
            }

            if (_dbContext.Cards.Any(card => card.IsActive && card.VisitorId == visitor.Id))
            {
                var message = "Visitor already has an active access card. Please deactivate it first.";
                _logger.LogError(message);
                throw new ArgumentException(message);
            }

            return RegisterCardInternal(visitor.Id);
        }

        private async Task<Card> RegisterCardInternal(int visitorId)
        {
            var newCard = await _dbContext.Cards.AddAsync(new Card
            {
                IsActive = true,
                IsCheckedIn = false,
                VisitorId = visitorId
            });
            await _dbContext.SaveChangesAsync();

            _logger.LogInformation($"Call RegisterCardAsync for visitor {visitorId} returned with {newCard.Entity.Id}");
            return newCard.Entity;
        }
    }
}
