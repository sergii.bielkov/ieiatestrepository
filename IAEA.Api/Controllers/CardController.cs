﻿using System;
using System.Threading.Tasks;
using IAEA.Api.Models;
using IAEA.Api.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IAEA.Api.Controllers
{
    [EnableCors("AllowAll")]
    [Route("api/[controller]")]
    [ApiController]
    public class CardController : ControllerBase
    {
        private readonly ITrackingService _trackingService;

        public CardController(ITrackingService trackingService)
        {
            _trackingService = trackingService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RegisterCardRequest request)
        {
            try
            {
                var result = await _trackingService.RegisterCardAsync(request.VisitorName);

                return CreatedAtAction(nameof(Post), null, new RegisterCardResponse { CardId = result.Id });
            }
            catch (ArgumentException exception)
            {
                return Ok(new RegisterCardResponse
                    {CardId = 0, IsFailure = true, ProblemDescription = exception.Message});
            }
        }
    }
}