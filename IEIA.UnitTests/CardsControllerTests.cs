using System;
using FluentAssertions;
using IAEA.Api.Controllers;
using IAEA.Api.Models;
using IAEA.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace IEIA.UnitTests
{
    public class CardsControllerTests
    {
        private readonly Mock<ITrackingService> _trackingServiceMock;
        private readonly MockRepository _mockRepository;

        public CardsControllerTests()
        {
            _mockRepository = new MockRepository(MockBehavior.Strict);
            _trackingServiceMock = _mockRepository.Create<ITrackingService>();
        }

        [Fact]
        public void Post_WithoutActiveCard_Success()
        {
            var requestData = new RegisterCardRequest
            {
                VisitorName = "Superman"
            };
            _trackingServiceMock.Setup(x => x.RegisterCardAsync(It.IsAny<string>())).ReturnsAsync(new Card {Id = 8});

            var sut = new CardController(_trackingServiceMock.Object);

            var actionResult = sut.Post(requestData).Result;

            actionResult.Should()
                .BeOfType<CreatedAtActionResult>();
        }

        [Fact]
        public void Post_WithActiveCard_Fail()
        {
            var requestData = new RegisterCardRequest
            {
                VisitorName = "Superman"
            };
            _trackingServiceMock.Setup(x => x.RegisterCardAsync(It.IsAny<string>())).Throws(
                new ArgumentException("Visitor already has an active access card. Please deactivate it first."));

            var sut = new CardController(_trackingServiceMock.Object);

            var actionResult = sut.Post(requestData).Result;

            actionResult.Should()
                .BeOfType<OkObjectResult>().Which.Value.Should().BeOfType<RegisterCardResponse>().Which
                .ProblemDescription.Equals("Visitor already has an active access card. Please deactivate it first.");
        }
    }
}
